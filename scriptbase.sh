#!/bin/sh

#
# TITLE:
#
# DESCRIPTION:
#
# by Your Name
#
# CHANGELOG:
#   0.0 yyyy/mm/dd スクリプト作成
#
SCRIPT_VER=0.0

#
# Script template.
# by Hanao Hanada
#
# Template changelog:
# 2016/06/19 &&を用いてif文を簡略化した．
# 2015/09/29 初期テンプレート作成．
#
TEMPLATE_VER=0.2


mypath=$(cd $(dirname $0) && pwd)
myname=$(basename $0)
myver=$SCRIPT_VER


#----------------------------------------------------------------------
# CUSTOM VARIABLES
#----------------------------------------------------------------------



#----------------------------------------------------------------------
# USAGE
#----------------------------------------------------------------------
function usage(){
  {
    cat <<__EOF__
$myname version $myver

Usage: $myname [OPTIONS] [PARAMETERS]

OPTIONS:
    -h, --help
        dispaly this help and exit.

PARAMETERS:
    not supported yet.
__EOF__
  } 1>&2
}


#----------------------------------------------------------------------
# FUNCTIONS
#----------------------------------------------------------------------
#==================================================
# Command line analysis
#==================================================
# [NOTE]: 先にオプション，後に引数の順でしか受けられない実装になっている．任意の順に
# 対応するには改良が必要．
option_d='nil'
arguments=()
function option_parser(){
  for opt in $*
  do
    case $opt in
      '-h'|'--help')
        usage; exit
        ;;
      '-d')
        option_d='t'
        ;;
      *)
        [ ! -z $1 ] && arguments+=($1)
        ;;
    esac
    shift
done
}

function option_getval(){
  _opt=$1
  _val=$(eval 'echo $option_'$_opt)
  echo $_val
  unset _opt _val
}

#==================================================
# Debugging
#==================================================
function dbg_p(){
  [ $(option_getval "d") = 't' ] && echo "[dbg] $*" >&2
}

#==================================================
# dummy
#==================================================
function dummy(){
  echo "dummy function."
}



#----------------------------------------------------------------------
# MAIN ROUTINE
#----------------------------------------------------------------------
#==================================================
# Command line analysis
#==================================================
cmdline=()
i=1
num=$#
while [ $i -le $num ]; do
  cmdline+=($1) ; shift
  i=$(($i + 1))
done

option_parser ${cmdline[*]}


#==================================================
# Some nice checkings...
#==================================================
[ $(option_getval "d") = 't' ] && dbg_p "option -d is set."
[ ! -z $arguments ] && dbg_p "args: ${arguments[*]}"


#==================================================
# Main part starts below.
#==================================================

echo "hello, world."
