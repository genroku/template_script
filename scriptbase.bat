@echo off

rem
rem TITLE:
rem
rem DESCRIPTION:
rem
rem by Your Name
rem
rem CHANGELOG:
rem yyyy/mm/dd comment.
rem 

set myver=0.0
set myname=%~n0%
set mypath=%~p0%

rem
rem [TEMPLATE]
rem by Hanao Hanada
rem Last modified : 2016/04/17 (since 2015/10/20)
rem

set template_ver=0.1



rem ----------------------------------------------------------------------
rem Custom variables
rem ----------------------------------------------------------------------



rem ----------------------------------------------------------------------
rem Main
rem ----------------------------------------------------------------------
set opt_d_flag=
set opt_p_para=
set params=

call :option_parser %*

echo DBG_d: %opt_d_flag%
echo DBG_p: %opt_p_para%
echo DBG__: %params%

pause
exit /b 0



rem ----------------------------------------------------------------------
rem Subroutines
rem ----------------------------------------------------------------------
rem ==================================================
rem [Sub] Print help
rem ==================================================
:print_help
  echo Usage: %myname% [OPTIONS] [PARAMETERS]
  echo OPTIONS:
  echo     /H
  echo        display this help and exit.
  echo     /D
  echo        dummy option.
  echo     /P val
  echo        sample option.
  echo PARAMETERS:
  echo     not supported yet.
exit /b


rem ==================================================
rem [Sub] Option parser
rem ==================================================
:option_parser
rem 全体がwhile文になっている(goto)

  set opt=%1
  shift

  if "%opt%" equ "" exit /b

  if %opt%==/H (
    call :print_help
    exit /b

  ) else if %opt%==/D (
    set opt_d_flag='t'

  ) else if %opt%==/P (
    set opt_p_para=%1
    shift

  ) else (
    if "%params%" equ "" (
      set params=%opt%
    ) else (
      set params=%params% %opt%
    )
  )

  goto :option_parser
exit /b
