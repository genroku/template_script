#!/usr/bin/python
# -*- coding: utf-8 -*-

#
# TITLE:
#
# DESCRIPTION:
#
# by Your Name
#
# CHANGELOG:
# yyyy/mm/dd 初期スクリプト作成．
#
SCRIPT_VER = 0.0

#
# Script template.
# by Hanao Hanada
#
# Last modified 2016/04/17 (since 2015/11/30(
#
TEMPLATE_VER = 0.1



import sys
from os import path
import copy


#--------------------------------------------------------------------
# Configurations
#--------------------------------------------------------------------
CVAR_SAMPLE = "sample"


#--------------------------------------------------------------------
# Global variables (for internal use)
#--------------------------------------------------------------------


#--------------------------------------------------------------------
# for debugging
#--------------------------------------------------------------------


#--------------------------------------------------------------------
# application specific class
#--------------------------------------------------------------------
#==================================================
# カスタム変数管理
#==================================================
class CustomVars:
    #[method]--------------------  
    def __init__(self):
        self.Sample = CVAR_SAMPLE


    #[method]--------------------  
    def Info(self):
        print(self.Sample)


#==================================================
# Script Info.
#==================================================
class ScriptSelf:
    #[method]--------------------  
    def __init__(self):
        self.Name = __file__
        self.FullPath = path.abspath(__file__)
        self.Path = self.FullPath.replace('/'+self.Name, '')
        self.Version = SCRIPT_VER

    #[method]--------------------  
    def Usage(self):
        print("""\
Usage:
    {self.Name} [OPTIONS][PARAMETERS]

OPTIONS:
    -h     display this help and exit.
    -d     for developer of {self.Name}
    -p val sample of option with value.

PARAMETERS:
    not supported.

version: {self.Version}
\
        """.format(**vars())) #←文字列内で変数展開を行なう

    #[method]--------------------  
    def Info(self):
        print("[SCRIPT]")
        print("Name: %s" %self.Name)
        print("Full: %s" %self.FullPath)
        print("Path: %s" %self.Path)
        print("Vers: %s" %self.Version)
        print("")


#==================================================
# Option analysis
#==================================================
class OptionManager:
    #[method]--------------------  
    def __init__(self, argv, script):
        argv.pop(0) #as shift
        self.Argv = argv
        self.Script = script
        self.Instructions = {"-h":False, "-d":False, "-p":""}
        self.Parameters = []
        self.Parse()

    #[method]--------------------  
    def Parse(self):
        argv = copy.deepcopy(self.Argv) #単純にargv=self.Argvとすると参照渡しになるので.pop()によりself.Argvのデータも無くなる．
        while len(argv) > 0:
            opt = argv.pop(0)
            if opt == "-h":
                self.Instructions["-h"] = True
                self.Script.Usage()
                sys.exit()
            elif opt == "-d":
                self.Instructions["-d"] = True
            elif opt == "-p":
                self.Instructions["-p"] = argv.pop(0)
            else:
                self.Parameters.append(opt)

    #[method]--------------------  
    def Info(self):
        print("[OPTIONS]")
        print("Argv: %s" %self.Argv)
        print("Inst: %s" %self.Instructions)
        print("Para: %s" %self.Parameters)
        print("")


#==================================================
# Application
#==================================================
class AppClass:
    #[method]--------------------  
    def __init__(self, opt, csvar):
        self.Opt = opt
        self.CsVar = csvar

    #[method]--------------------  
    def Main(self):
        print("this is user application class.")

    #[method]--------------------  
    def Info(self):
        pass


#--------------------------------------------------------------------
# Main routine
#--------------------------------------------------------------------

if __name__ == "__main__":

    MyScpt = ScriptSelf()

    argv = sys.argv
    MyOptM = OptionManager(argv, MyScpt)

    if MyOptM.Instructions["-d"] :
        MyOptM.Info()
        MyScpt.Info()

    MyCVar = CustomVars()
    MyApp = AppClass(MyOptM, MyCVar)
    MyApp.Main()
