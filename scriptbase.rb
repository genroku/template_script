#!/usr/bin/ruby
# -*- coding: utf-8 -*-

#
# TITLE:
#
# DESCRIPTION:
#
# by Your Name
#
# CHANGELOG:
# yyyy/mm/dd 初期スクリプト作成．
#
SCRIPT_VER = 0.0

#
# Script template.
# by Hanao Hanada
#
# Last modified 2016/04/17 (since 2015/03/11)
#
TEMPLATE_VER = 0.3



require 'optparse'


#--------------------------------------------------------------------
# Configurations
#--------------------------------------------------------------------


#--------------------------------------------------------------------
# Global variables (for internal use)
#--------------------------------------------------------------------


#--------------------------------------------------------------------
# for debugging
#--------------------------------------------------------------------
def dbg_p(string)
    if( $MyOption.IsDbgMode )
        print "(dbg) " + string
    end
end #def


#--------------------------------------------------------------------
# application specific class
#--------------------------------------------------------------------

#==================================================
# Script Info.
#==================================================
class ScriptSelf
  attr_accessor :Version

  #[method]--------------------  
  def initialize
    @Version = SCRIPT_VER
    @Name = File.basename(__FILE__)
    @Path = File.dirname(__FILE__)
    @Path = File.expand_path(@Path)
  end #def

  #[method]--------------------  
  def Usage()
    print <<-__USAGE__

#{@Name} [OPTIONS][PARAMETERS]
        
OPTIONS:
    -h, --help  display this help and exit.
    -d, --debug for developer of "#{@Name}"
    -p val      sample of option with value.        

PARAMETERS:
    not supported.

version: #{@Version}
__USAGE__
  end #def

  #[method]--------------------  
  def Info()
    print "[SCRIPT]\n"
    print "Name: #{@Name}\n"
    print "Path: #{@Path}\n"
    print "Version: #{@Version}\n"
    print "\n"
  end #def

end #class


#==================================================
# Option analysis
#==================================================
class OptionManager
 attr_accessor :IsDbgMode, :Parameters
 
  #[method]--------------------
  def initialize(argv, script)
    @Script = script

    Parse(argv)
    @Parameters = argv

    @IsDbgMode = @Instructs['-d']

    if( @Instructs['-h'] )
      @Script.Usage()
      GoodBye()
    end #if
  end #def

  #[method]--------------------
  def Parse(argv)
    @Instructs = {
      '-h'=>false,
      '-d'=>false, 
      '-p'=>""
    }

    OptionParser.new {|opt|
      #option definition part
      opt.on('-h', '--help')  {|v| @Instructs['-h'] = v}
      opt.on('-d', '--debug') {|v| @Instructs['-d'] = v}
      opt.on('-p VALUE')      {|v| @Instructs['-p'] = v}

      #do option parse
      begin
        opt.parse!(argv)
      rescue
        print "unknown option.\n"
        @Script.Usage()
        GoodBye()
      end #begin
    }
  end #def

  #[method]--------------------  
  def Info
    print "[OPTION]\n"
    @Instructs.each{|key, val|
      print "#{key}: #{val}\n"
    }
    print "\n"
  end #def

  #[method]--------------------
  def GoodBye
    print "bye.\n"
    exit
  end #def

  private :Parse, :GoodBye

end #class


#==================================================
# Application
#==================================================
class AppClass

  #[method]--------------------
  def initialize(opt)
    @Opt = opt
  end #def

  #[method]--------------------
  def Main()
    file_list = @Opt.Parameters
    for f in file_list
      if( !File.exist?(f) )
        print "file not found: #{f}\n"
      else
        print "file : #{f}\n"
        FileExec(f)
      end #if
      print "\n"
    end #for
  end #def

  #[method]--------------------
  def FileExec(file)
    dbg_p "open file #{file}\n"
    fd = open(file, "r")
    fd.each {|l|
      PrintLine(l)
    }
    fd.close()
  end #def

  #[method]--------------------
  def PrintLine(line)
    p line
  end

  private :FileExec, :PrintLine
end #class


#--------------------------------------------------------------------
# Main routine
#--------------------------------------------------------------------

### NO NEED TO CHANGE BELOW -----------------------

$MyScript = ScriptSelf.new()
$MyOption = OptionManager.new(ARGV, $MyScript)

if( $MyOption.IsDbgMode )
  dbg_p "DEBUG MODE.\n"
  $MyScript.Info()
  $MyOption.Info()
end #if


$MyApp = AppClass.new($MyOption)
$MyApp.Main()
